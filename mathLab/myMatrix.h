//Introductory matrix class

#include <math.h>

#include <GL/gl.h>            // standard OpenGL include
#include <GL/glu.h>           // OpenGL utilties
#include <GL/glut.h>             // OpenGL utilties


namespace MyMathLab
{

class MyMatrix
{
    public:
        MyMatrix(void);
        ~MyMatrix(void){;}

        void loadIdentity(void);

        void getGLModelviewMatrix(void);
        void getGLProjectionMatrix(void);

        void multiplyGLMatrix(void);
        void setGLMatrix(void);
    
    private:

        void getGLMatrix(GLenum pname);

        GLfloat myMatrix[16];
};


}
